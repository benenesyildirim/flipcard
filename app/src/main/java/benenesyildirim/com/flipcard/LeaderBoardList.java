package benenesyildirim.com.flipcard;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class LeaderBoardList extends AppCompatActivity {

    private RecyclerView recyclerView;
    private List<FireBaseUser> leaderBoardUsers = new ArrayList<>();
    private FireBaseUser user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leader_board_list);

        getHighScores();
    }

    private void getHighScores() {
        recyclerView = findViewById(R.id.recyclerViewLB);

        DatabaseReference rootRef = FirebaseDatabase.getInstance().getReference();
        DatabaseReference usersRef = rootRef.child("users");
        ValueEventListener eventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    long highScore = (long) ds.child("highScore").getValue();
                    String name = (String) ds.child("userName").getValue();
                    String email = (String) ds.child("userEmail").getValue();
                    user = new FireBaseUser(name, email, highScore);
                    leaderBoardUsers.add(user);
                }
                Collections.sort(leaderBoardUsers, Collections.reverseOrder());

                LeaderBoardListAdapter leaderBoardListAdapter = new LeaderBoardListAdapter(LeaderBoardList.this, leaderBoardUsers);
                recyclerView.setAdapter(leaderBoardListAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(LeaderBoardList.this, R.string.check_internet, Toast.LENGTH_SHORT).show();
            }
        };
        usersRef.addListenerForSingleValueEvent(eventListener);
    }
}