package benenesyildirim.com.flipcard;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class LeaderBoardListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<FireBaseUser> leaderBoardList;
    private Context context;

    LeaderBoardListAdapter(Context context, List<FireBaseUser> leaderBoardList) {
        this.context = context;
        this.leaderBoardList = leaderBoardList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.leaderboard_row_design, viewGroup, false);
        return new LeaderBoardListAdapter.ListHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        final LeaderBoardListAdapter.ListHolder listHolder = (ListHolder) viewHolder;
        final String userName = leaderBoardList.get(i).getUserName();
        final long highScore = leaderBoardList.get(i).getHighScore();

        listHolder.numberText.setText(String.valueOf(i + 1));
        listHolder.scoreText.setText(String.valueOf(highScore));
        listHolder.userNameText.setText(userName);
    }

    @Override
    public int getItemCount() {
        return leaderBoardList.size();
    }

    public class ListHolder extends RecyclerView.ViewHolder {

        TextView numberText, scoreText, userNameText;

        ListHolder(@NonNull View v) {
            super(v);
            numberText = v.findViewById(R.id.number_txt);
            scoreText = v.findViewById(R.id.score_txt);
            userNameText = v.findViewById(R.id.username_txt);
        }
    }
}