package benenesyildirim.com.flipcard;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class StartScreen extends AppCompatActivity implements View.OnClickListener {

    private FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
    private DatabaseReference databaseReference;
    private SharedPreferences sharedPreferences;
    private String userName, email;
    private TextView userNameTitle, scoreTitle, highScoreTitle;
    private InterstitialAd mInterstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_screen);

        sharedPreferences = getSharedPreferences("preferences", MODE_PRIVATE);
        initAds();
        initFirebase();
        initView();
        fillViewIfUserCreated();
    }

    private void initAds() {
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-8661505007193806/4551051191");
        requestNewInterstitial();
        mInterstitialAd.setAdListener(new AdListener() { //reklamımıza listener ekledik ve kapatıldığında haberimiz olacak
            @Override
            public void onAdClosed() { //reklam kapatıldığı zaman tekrardan reklamın yüklenmesi için
                Toast.makeText(StartScreen.this, "Forwarding To Game...", Toast.LENGTH_SHORT).show();
                showUserCreateDialog();
                requestNewInterstitial();
            }
        });
    }

    private void initView() {
        userNameTitle = findViewById(R.id.welcomeTitle);
        scoreTitle = findViewById(R.id.scoreTitle);
        highScoreTitle = findViewById(R.id.highScoreTitle);
        // Button leaderboardBtn = findViewById(R.id.leaderboard_btn); kullanılmadığı için deactive edildi.
    }

    private void fillViewIfUserCreated() {
        sharedPreferences = getSharedPreferences("preferences", MODE_PRIVATE);
        if (!sharedPreferences.getString("userID", "").isEmpty()) {
            DatabaseReference user = firebaseDatabase.getReference("users").child(sharedPreferences.getString("userID", ""));
            user.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {//Kullanıcının bilgilerini ana ekrana alıyoruz, eğer varsa!
                        FireBaseUser value = dataSnapshot.getValue(FireBaseUser.class);
                        userNameTitle.setText(getString(R.string.start_screen_welcome_txt, value.getUserName()));
                        scoreTitle.setText(getString(R.string.start_screen_totalscore_txt, value.getTotalScore()));
                        highScoreTitle.setText(getString(R.string.start_screen_highscore_txt, value.getHighScore()));
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {//İnternet yoksa.
                    Toast.makeText(StartScreen.this, getString(R.string.check_internet), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    private void initFirebase() {
        databaseReference = firebaseDatabase.getReference("users");
        FirebaseApp.initializeApp(getApplicationContext());
    }

    @Override
    public void onClick(View view) {// XML koduyla listener eklendi.
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        }
    }

    public void onClickListing(View view) {// Leaderboard Button'a XML koduyla listener eklendi.
            Intent intent = new Intent(StartScreen.this, LeaderBoardList.class);
            startActivity(intent);
    }

    private void showUserCreateDialog() {
        String userID = sharedPreferences.getString("userID", "");
        assert userID != null;
        if (userID.isEmpty()) {
            AlertDialog.Builder mBuilder = new AlertDialog.Builder(this);
            View v = getLayoutInflater().inflate(R.layout.create_user_dialog, null);
            final EditText userET = v.findViewById(R.id.user_nameET);
            final EditText emailET = v.findViewById(R.id.user_emailET);
            Button addBtn = v.findViewById(R.id.addBtn);

            mBuilder.setView(v);
            final AlertDialog dialog = mBuilder.create();
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
            addBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!userET.getText().toString().isEmpty() && !emailET.getText().toString().isEmpty()) {
                        userName = userET.getText().toString();
                        email = emailET.getText().toString();
                        createUser(userName, email);
                        dialog.dismiss();
                    }
                }
            });
        } else {
            Intent playGame = new Intent(this, FlipCard.class);
            startActivity(playGame);
        }
    }

    private void createUser(String userName, String email) {
        String userID = databaseReference.push().getKey();

        SharedPreferences.Editor editor = getSharedPreferences("preferences", MODE_PRIVATE).edit();
        editor.putString("userID", userID);
        editor.apply();

        FireBaseUser user = new FireBaseUser(userName, email, "", sharedPreferences.getLong("score", 0L), 0L);
        assert userID != null;
        databaseReference.child(userID).setValue(user);//User eklendi.

        fillViewIfUserCreated();
    }

    private void requestNewInterstitial() {
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice("B9C840C4E9AD8EC5D1497C9A62C56374")
                .build();

        mInterstitialAd.loadAd(adRequest);
    }
}