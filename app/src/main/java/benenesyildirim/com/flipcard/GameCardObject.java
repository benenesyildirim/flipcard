package benenesyildirim.com.flipcard;


import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;
import android.widget.ImageView;

public class GameCardObject extends android.support.v7.widget.AppCompatImageView {

    private int id, frontCard;

    public GameCardObject(Context context) {
        super(context);
    }

    public GameCardObject(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public GameCardObject(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public GameCardObject(Context context, int id, int frontCard) {
        super(context);
        this.frontCard = frontCard;
        this.id = id;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public void setId(int id) {
        this.id = id;
    }

    public int getFrontCard() {
        return frontCard;
    }
}