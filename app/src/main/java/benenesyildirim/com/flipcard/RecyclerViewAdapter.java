package benenesyildirim.com.flipcard;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import java.util.List;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<GameCardObject> gameCardObjectList;
    private ClickListener clickListener;

    RecyclerViewAdapter(List<GameCardObject> gameCardObjectList, ClickListener clickListener) {
        this.gameCardObjectList = gameCardObjectList;
        this.clickListener = clickListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = (LayoutInflater) viewGroup.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.card_design, null);
        return new ListHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int i) {
        final ListHolder listHolder = (ListHolder) viewHolder;
        final GameCardObject cardObject = gameCardObjectList.get(i);
        listHolder.frontWall.setImageResource(cardObject.getFrontCard());

        listHolder.gameCardFL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (clickListener != null) {
                    clickListener.cardClickListener(listHolder.backWall, listHolder.gameCardFL, cardObject);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return gameCardObjectList.size();
    }

    public class ListHolder extends RecyclerView.ViewHolder {

        FrameLayout gameCardFL;
        AppCompatImageView backWall;
        GameCardObject frontWall;

        ListHolder(@NonNull View v) {
            super(v);

            gameCardFL = v.findViewById(R.id.gameCardFL);
            backWall = v.findViewById(R.id.backWall);
            frontWall = v.findViewById(R.id.frontWall);
        }
    }

    public interface ClickListener {
        void cardClickListener(AppCompatImageView cardsImageView, FrameLayout cardFL, GameCardObject gameCardObject);
    }
}
