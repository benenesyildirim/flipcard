package benenesyildirim.com.flipcard;


import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.CountDownTimer;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.Stack;
import java.util.concurrent.TimeUnit;

public class FlipCard extends AppCompatActivity implements RecyclerViewAdapter.ClickListener {

    private static final String FORMAT = "%02d:%02d";

    private RecyclerView recyclerView;
    private List<Integer> listOfImages;
    private List<GameCardObject> gameCardObjectList = new ArrayList<>();
    private Stack<ImageView> imageViewStack = new Stack<>();
    private List<Integer> remainingMove = new ArrayList<>();
    private Stack<Integer> integerStack = new Stack<>();
    private Stack<FrameLayout> frameLayoutStack = new Stack<>();
    private boolean needToCloseCards;
    private int level = 1, matchedCards = 0, move = 0, totalMove;
    private TextView levelTV, scoreTV, timerTV;
    private long remainingTime;
    private int score;
    private CountDownTimer countDownTimer;

    private enum GameOverEnum {
        SUCCESS, FAILURE, GAMEOVER, TIMESUP, HIGHSCORE;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flip_card);

        initView();
        createListOfImages();
        createForLevel();
        fillView();
        initializeTimer();
    }

    private void initializeTimer() {
        countDownTimer = new CountDownTimer(180000, 1000) { // Timer ayarlanıyor.

            @SuppressLint({"DefaultLocale", "SetTextI18n"})
            public void onTick(long millisUntilFinished) {
                remainingTime = millisUntilFinished;
                timerTV.setText("Time\n" + String.format(FORMAT, TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) -
                                TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millisUntilFinished)),
                                TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));
            }

            public void onFinish() {
                timerTV.setText(R.string.gameOver);
                showFinishDialog(GameOverEnum.TIMESUP);
            }
        };
        countDownTimer.start();
    }

    private void initView() {
        recyclerView = findViewById(R.id.recyclerView);
        levelTV = findViewById(R.id.levelTV);
        scoreTV = findViewById(R.id.scoreTV);
        timerTV = findViewById(R.id.countDownTimerTV);
        levelTV.setText("LeveL\n1");
    }

    private void createForLevel() {
        move = getMoveCount();
        gameCardObjectList = new ArrayList<>();
        for (int i = 0; i < getCardCount(); i++) {
            createGameObjects(i);
        }
        duplicateAndShuffleGameObjects();
    }

    private void fillView() {
        RecyclerViewAdapter recyclerViewAdapter = new RecyclerViewAdapter(gameCardObjectList, FlipCard.this);
        recyclerView.setLayoutManager(new GridLayoutManager(getApplicationContext(), getSpanCount()));
        recyclerView.setAdapter(recyclerViewAdapter);
    }

    private void duplicateAndShuffleGameObjects() {
        List<GameCardObject> duplicatedGameCardObjectList = new ArrayList<>(gameCardObjectList);
        gameCardObjectList.addAll(duplicatedGameCardObjectList);
        Collections.shuffle(gameCardObjectList);
    }

    private void createGameObjects(int i) {
        int max = listOfImages.size();//listOfImages.size() - 1 , min = 0
        int random = new Random().nextInt(max);//(max - min) + 1)
        int randomImage = listOfImages.get(random);
        GameCardObject cardObject = new GameCardObject(getApplicationContext(), i, randomImage);
        gameCardObjectList.add(cardObject);
        listOfImages.remove(random);
    }

    private void createListOfImages() {
        listOfImages = new ArrayList<>();

        listOfImages.add(R.mipmap.baboon);
        listOfImages.add(R.mipmap.bear);
        listOfImages.add(R.mipmap.bear2);
        listOfImages.add(R.mipmap.bear3);
        listOfImages.add(R.mipmap.bird);
        listOfImages.add(R.mipmap.boar);
        listOfImages.add(R.mipmap.bull);
        listOfImages.add(R.mipmap.camel);
        listOfImages.add(R.mipmap.cat);
        listOfImages.add(R.mipmap.cat2);
        listOfImages.add(R.mipmap.cat3);
        listOfImages.add(R.mipmap.chicken);
        listOfImages.add(R.mipmap.cobra);
        listOfImages.add(R.mipmap.cow);
        listOfImages.add(R.mipmap.crab);
        listOfImages.add(R.mipmap.crocodile);
        listOfImages.add(R.mipmap.dog);
        listOfImages.add(R.mipmap.dog2);
        listOfImages.add(R.mipmap.dog3);
        listOfImages.add(R.mipmap.duck);
        listOfImages.add(R.mipmap.elephant);
        listOfImages.add(R.mipmap.fox);
        listOfImages.add(R.mipmap.fox2);
        listOfImages.add(R.mipmap.frog);
        listOfImages.add(R.mipmap.giraffe);
        listOfImages.add(R.mipmap.goat);
        listOfImages.add(R.mipmap.gorilla);
        listOfImages.add(R.mipmap.hippopotamus);
        listOfImages.add(R.mipmap.horse);
        listOfImages.add(R.mipmap.koala);
        listOfImages.add(R.mipmap.lion);
        listOfImages.add(R.mipmap.lioness);
        listOfImages.add(R.mipmap.mole);
        listOfImages.add(R.mipmap.monkey);
        listOfImages.add(R.mipmap.monkey2);
        listOfImages.add(R.mipmap.mouse);
        listOfImages.add(R.mipmap.owl);
        listOfImages.add(R.mipmap.owl2);
        listOfImages.add(R.mipmap.panther);
        listOfImages.add(R.mipmap.parrot);
        listOfImages.add(R.mipmap.parrot2);
        listOfImages.add(R.mipmap.parrot3);
        listOfImages.add(R.mipmap.pig);
        listOfImages.add(R.mipmap.pigeon);
        listOfImages.add(R.mipmap.polar_bear);
        listOfImages.add(R.mipmap.rabbit);
        listOfImages.add(R.mipmap.reindeer);
        listOfImages.add(R.mipmap.rhinoceros);
        listOfImages.add(R.mipmap.sea_lion);
        listOfImages.add(R.mipmap.shark);
        listOfImages.add(R.mipmap.sheep);
        listOfImages.add(R.mipmap.snake);
        listOfImages.add(R.mipmap.squirrel);
        listOfImages.add(R.mipmap.tiger);
        listOfImages.add(R.mipmap.tiger2);
        listOfImages.add(R.mipmap.tiger3);
        listOfImages.add(R.mipmap.unicorn);
        listOfImages.add(R.mipmap.zebra);
    }

    @Override
    public void cardClickListener(AppCompatImageView cardImageView, FrameLayout cardFL, GameCardObject gameCardObject) {
        if (cardFL.getTag() != null && (boolean) cardFL.getTag()) {
            Toast.makeText(getApplicationContext(), "Can't Touch This", Toast.LENGTH_SHORT).show();
        } else {
            if (needToCloseCards) {
                closeCards();
            }

            openAnimation(cardImageView);
            cardFL.setTag(true);

            imageViewStack.push(cardImageView);
            integerStack.push(gameCardObject.getId());
            frameLayoutStack.push(cardFL);

            if (imageViewStack.size() == 2) {

                levelTV.setText("Move\n" + String.valueOf(move));

                int secondCardId = integerStack.pop();
                int firstCardId = integerStack.pop();
                if (firstCardId != secondCardId) {
                    move -= 1;
                    needToCloseCards = true;
                } else {
                    imageViewStack.clear();
                    integerStack.clear();
                    matchedCards++;
                    Toast.makeText(this, "It's a Match!", Toast.LENGTH_SHORT).show();
                    score += level + 5; // Her doğruda ne kadar puan ekleniyor.

                    scoreTV.setText(getString(R.string.score_text, score));
                    if (level == 4) {
                        if (matchedCards == getCardCount()) {
                            remainingMove.add(move);
                            collectorOfMoves();
                            score += (totalMove * 3) + (int) (remainingTime / 1000);// Skora hareket ve süreden kalanlar ekleniyor.
                            gameOver();
                        }
                    } else {
                        if (matchedCards == getCardCount()) {
                            remainingMove.add(move);
                            newLevel();
                        }
                    }
                }
            }

            if (move == 0) {
                countDownTimer.cancel();// Score'un kaydedilmesi için gameOver fonksiyonunu çalışıtrmalıyız.
                showFinishDialog(GameOverEnum.FAILURE);
            }

        }
    }

    private void gameOver() {
        countDownTimer.cancel();
        levelTV.setText("GAME");
        timerTV.setText("");
        scoreTV.setText("OVER");

        // Firebase'e bağlantı gerçekleştiriyouz.
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference reference = database.getReference("users");
        SharedPreferences sharedPreferences = getSharedPreferences("preferences", MODE_PRIVATE);
        SharedPreferences.Editor editor = getSharedPreferences("preferences", MODE_PRIVATE).edit();
        String userID = sharedPreferences.getString("userID", "");

        long savedScore = sharedPreferences.getLong("score", 0L);
        savedScore = score + savedScore;

        assert userID != null;// UserID null check.
        reference.child(userID).child("totalScore").setValue(savedScore);
        saveScore(savedScore);
        long highScore = sharedPreferences.getLong("highScore", 0L);

        if (highScore == 0) {
            reference.child(userID).child("highScore").setValue(score);
            editor.putLong("highScore", (long) score);
            editor.apply();
            showFinishDialog(GameOverEnum.HIGHSCORE);
        } else {
            if (score > highScore) {
                reference.child(userID).child("highScore").setValue(score);
                editor.putLong("highScore", (long) score);
                showFinishDialog(GameOverEnum.HIGHSCORE);
            } else {
                showFinishDialog(GameOverEnum.GAMEOVER);// TODO Buraya success de konulabilir.
            }
        }
    }

    private void newLevel() {
        matchedCards = 0;
        level++;
        levelTV.setText(getString(R.string.level_text, level));
        createListOfImages();
        createForLevel();
        fillView();
    }

    private void closeCards() {//TODO Stacklere bi bak
        ImageView firstIV = imageViewStack.pop();
        ImageView secondIV = imageViewStack.pop();
        closeAnimation(firstIV);
        closeAnimation(secondIV);
        firstIV.setVisibility(View.VISIBLE);
        secondIV.setVisibility(View.VISIBLE);

        FrameLayout firstFL = frameLayoutStack.pop();
        FrameLayout secondFL = frameLayoutStack.pop();
        firstFL.setTag(false);
        secondFL.setTag(false);

        needToCloseCards = false;
    }

    private int getSpanCount() {//Bir sıradaki kart sayısı
        if (level == 1) {
            return 3;
        } else {
            return 4;
        }
    }

    private int getCardCount() {
        switch (level) {
            case 1:
                return 3;
            case 2:
                return 4;
            case 3:
                return 6;
            case 4:
                return 8;
            case 5:
                return 10;
            default:
                return 3;
        }
    }

    private void closeAnimation(final ImageView imageView) {
        ObjectAnimator flip = ObjectAnimator.ofFloat(imageView, "rotationY", 90f, 0f);
        flip.setDuration(250);
        flip.start();
    }

    private void openAnimation(final ImageView imageView) {
        ObjectAnimator frontFlip = ObjectAnimator.ofFloat(imageView, "rotationY", 0f, -90f);
        //ObjectAnimator backFlip = ObjectAnimator.ofFloat(imageView, "rotationY", 0f, 90f);

        frontFlip.setDuration(250);
        //backFlip.setDuration(100);

        frontFlip.start();
        //backFlip.start();
    }

    private void saveScore(long savedScore) {
        SharedPreferences.Editor editor = getSharedPreferences("preferences", MODE_PRIVATE).edit();
        editor.putLong("score", savedScore);
        editor.apply();
    }

    private void showFinishDialog(GameOverEnum gameOverEnum) {
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(FlipCard.this);
        @SuppressLint("InflateParams") View v = getLayoutInflater().inflate(R.layout.finish_dialog, null);
        mBuilder.setView(v);
        final AlertDialog dialog = mBuilder.create();
        Button addBtn = v.findViewById(R.id.popup_btn);
        ImageView image = v.findViewById(R.id.popup_iv);
        TextView scoreTV = v.findViewById(R.id.popup_score);
        scoreTV.setText(getString(R.string.scoreTV_txt, score));

        switch (gameOverEnum) {
            case SUCCESS:
                image.setImageResource(R.drawable.well_done);
                break;
            case FAILURE:
                scoreTV.setText(getString(R.string.scoreTV_txt, 0));
                image.setImageResource(R.drawable.failure);
                break;
            case TIMESUP:
                scoreTV.setText(getString(R.string.scoreTV_txt, 0));
                image.setImageResource(R.drawable.game_over);
                break;
            case GAMEOVER:
                image.setImageResource(R.drawable.game_over);
                break;
            case HIGHSCORE:
                image.setImageResource(R.drawable.high_score);
                break;
        }
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(FlipCard.this, "Forwarding To Start Screen...", Toast.LENGTH_SHORT).show();
                Intent playGame = new Intent(FlipCard.this, StartScreen.class);
                startActivity(playGame);
                ActivityCompat.finishAffinity(FlipCard.this);
                dialog.dismiss();
            }
        });
    }

    private int getMoveCount() {// Her Level için gerekli hsmle sayısını döndürür.
        switch (level) {
            case 1:
                return 6;
            case 2:
                return 10;
            case 3:
                return 15;
            case 4:
                return 22;
            case 5:
                return 27;
            default:
                return 5;
        }
    }

    private void collectorOfMoves() {
        for (int i = 0; i < remainingMove.size(); i++) {
            totalMove += remainingMove.get(i);
        }
    }
}