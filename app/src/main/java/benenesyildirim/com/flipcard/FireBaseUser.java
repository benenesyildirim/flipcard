package benenesyildirim.com.flipcard;

import java.io.Serializable;

public class FireBaseUser implements Serializable, Comparable<FireBaseUser> {

    private static final long serialVersionUID = 8759972585096560382L;

    private String userName, userEmail, pushToken;
    private Long totalScore, highScore;

    public FireBaseUser() {

    }

    public FireBaseUser(String userName, String userEmail, long highScore) {
        this.userName = userName;
        this.userEmail = userEmail;
        this.highScore = highScore;
    }

    public FireBaseUser(String userName, String userEmail, String pushToken, Long totalScore, Long highScore) {
        this.userName = userName;
        this.userEmail = userEmail;
        this.pushToken = pushToken;
        this.totalScore = totalScore;
        this.highScore = highScore;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPushToken() {
        return pushToken;
    }

    public void setPushToken(String pushToken) {
        this.pushToken = pushToken;
    }

    public Long getTotalScore() {
        return totalScore;
    }

    public void setTotalScore(Long totalScore) {
        this.totalScore = totalScore;
    }

    public Long getHighScore() {
        return highScore;
    }

    public void setHighScore(Long highScore) {
        this.highScore = highScore;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    @Override
    public int compareTo(FireBaseUser f) {
        if (highScore.floatValue() > f.highScore.floatValue()) {
            return 1;
        }
        else if (highScore.floatValue() <  f.highScore.floatValue()) {
            return -1;
        }
        else {
            return 0;
        }
    }
}
